package com.example.battlogger

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import java.io.*


class MainActivity : AppCompatActivity() {
    lateinit var pendingIntent: PendingIntent
    lateinit var btnStartService: Button
    lateinit var btnStopService:Button
    var alarmManager: AlarmManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnStartService = findViewById(R.id.buttonStartService)
        btnStopService = findViewById(R.id.buttonStopService)
        btnStartService.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                startService()
            }
        })
        btnStopService.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                stopService()
            }
        })
    }

    fun startService() {
        val delayBatt: Long = 9 * 60000 //milliseconds
        alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val currTime = System.currentTimeMillis()
        MyAlarm.filename = "default$currTime"
        MyAlarm.delay = delayBatt
        registerAlarm(this, System.currentTimeMillis())
    }


    fun stopService() {
        stopAlarm(this)
    }

    companion object{
        var pendingIntent : PendingIntent? = null
        fun registerAlarm(ctx: Context, time : Long){
            val intent = Intent(ctx, MyAlarm::class.java)
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND)
            pendingIntent = PendingIntent.getBroadcast(ctx, 0, intent,PendingIntent.FLAG_CANCEL_CURRENT )
            val alarmManager = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                time,
                pendingIntent
            )
        }

        fun stopAlarm(ctx: Context){
            if(pendingIntent != null) {
                val alarmManager = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                alarmManager.cancel(pendingIntent)
            }
        }
    }

    class MyAlarm : BroadcastReceiver(){
        companion object{
            lateinit var filename :String
            var delay : Long = 0
        }
        override fun onReceive(context: Context?, intent: Intent?) {
            if(context == null){
                Log.e("battLog", "Error triggering alarm, bad context")
                return
            }
            registerAlarm(context, System.currentTimeMillis() + delay)
            val appCtx =  context.applicationContext
            val bm = appCtx.getSystemService(Context.BATTERY_SERVICE) as BatteryManager
            val batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY)
            val currTime = System.currentTimeMillis()
            Log.d("Battery", "batt : $batLevel @ $currTime")
            val file =
                File(appCtx.getExternalFilesDir(null), "$filename.csv")
            var fileOutputStream: FileOutputStream? = null
            try {
                fileOutputStream = FileOutputStream(file, true)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            val outputStreamWriter =
                OutputStreamWriter(fileOutputStream)
            val bufferedWriter =
                BufferedWriter(outputStreamWriter)
            try {
                bufferedWriter.write("$currTime,$batLevel\n")
                bufferedWriter.flush()
                outputStreamWriter.flush()
                bufferedWriter.close()
                outputStreamWriter.close()
                fileOutputStream!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }
}